use std::time::Instant;

use glutin::{
    event::{Event, WindowEvent},
    event_loop::ControlFlow,
};
use taskbar_interface::TaskbarInterface;

fn main() {
    let el = glutin::event_loop::EventLoop::new();
    let mut indicator =
        TaskbarInterface::new().unwrap();
    #[cfg(all(unix, not(target_os = "macos")))]
    let _ = indicator.set_unity_app_uri("application://myapp.desktop");

    let start = Instant::now();

    el.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                window_id: _,
            } => *control_flow = ControlFlow::Exit,
            Event::MainEventsCleared => {
                indicator
                    .needs_attention(start.elapsed().as_secs() % 10 <= 5)
                    .unwrap();
            }
            _ => (),
        }
    });
}
